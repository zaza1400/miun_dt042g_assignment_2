/*
 * Autor       : Mikael Nilsson
 * Filename    : main.cpp
 * Description : C++ Screen example
 * Version     : 0.2
 *
*/

#include <memstat.hpp>
#include "screen.h"
#include <iostream>

using namespace std;

int main()
{
    Terminal terminal;
    Screen screen(80, 24);
    screen.fill(' ', TerminalColor(COLOR::GREEN, COLOR::BLUE));
    screen.fillRect(1,1,10,12,'A', COLOR::RED);
    screen.setTextRect(2,10,10,10, "Hello World!", COLOR::RED);
    screen.draw(terminal);
    return 0;
}

